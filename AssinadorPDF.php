<?php

// URL PARA A INICIALIZAÇÃO DA ASSINATURA PDF
const URLInicializacao = 'https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar';
// URL PARA A FINALIZAÇÃO DA ASSINATURA PDF
const URLFinalizacao = 'https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar';
// CAMINHO ONDE ESTÁ LOCALIZADO O ARQUIVO PDF A SER ASSINADO
const caminhoDoArquivoParaAssinar = '/caminhos/para/o/arquivo.pdf';
// TOKEN AUTHORIZATION GERADO NO BRY CLOUD
const token = "tokenAuthorization";
// CAMINHO ONDE ESTÁ LOCALIZADO O CERTIFICADO PKCS12
const caminhoDoCertificado = '/caminho/para/o/certificado.p12';
// SENHA DO CERTIFICADO
const senhaDoCertificado = 'senhaDoCertificado';


function inicializarAssinatura()
{

  // COLETA BASE64 DO CERTIFICADO 
  list($certificadoTratado) = getCertificadoEChavePrivada();

  // CRIAÇÃO DA REQUISIÇÃO QUE SERÁ ENVIADA PARA A INICIALIZAÇÃO
  $curlInicializacao = curl_init();

  curl_setopt_array($curlInicializacao, array(
    CURLOPT_URL => URLInicializacao,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array('documento' => new CURLFILE(caminhoDoArquivoParaAssinar), 'dados_inicializar' => '{"perfil" : "timeStamp",
"algoritmoHash" : "SHA256",
"formatoDadosEntrada" : "Base64",
"formatoDadosSaida" : "Base64",
"certificado": "' . $certificadoTratado . '",
"nonces": ["asdasdasd"]
}'),
    CURLOPT_HTTPHEADER => array(
      "Authorization:" . token
    ),
  ));

  // ENVIA A REQUISIÇÃO
  $respostaInicializacao = curl_exec($curlInicializacao);
  curl_close($curlInicializacao);

  echo "RESPOSTA DA INICIALIZAÇÃO:\n\n";
  echo $respostaInicializacao;
  echo "\n\n__________________________________________________\n\n";
  return $respostaInicializacao;
}

function cifraInicialização($respostaInicializacao)
{

  // COLETA A CHAVE PRIVADA DO CERTIFICADO
  list($messageDigest) = getContentRespostaInicializacao($respostaInicializacao);
  
  // COLETA A CHAVE PRIVADA DO CERTIFICADO
  list(,$privkey) = getCertificadoEChavePrivada();

  // DECODIFICA O BASE64 QUE ESTÁ NA VARIÁVEL "messageDigest"
  $messageDigestDecodificado = base64_decode($messageDigest);

  // CIFRA/ASSINA O CONTEÚDO QUE RETORNA DA INICIALIZAÇÃO COM A CHAVE PRIVADA DO CERTIFICADO
  $success = openssl_sign($messageDigestDecodificado, $crypted, $privkey, 'sha256WithRSAEncryption');

  // CODIFICA EM BASE64 O CONTEÚDO RETORNADO DA CIFRAGEM DOS DADOS
  $cifrado = base64_encode($crypted);
  echo "CONTEÚDO CIFRADO:\n\n";
  echo $cifrado;
  echo "\n\n_________________________________________________\n\n";
  return $cifrado;
}

function finalizarAssinatura($cifrado, $respostaInicializacao)
{
  // COLETA O NONCE QUE RETORNOU DA INICIALIZAÇÃO DA ASSINATURA
  list(,$nonce) = getContentRespostaInicializacao($respostaInicializacao);

  // CRIA O JSON QUE SERÁ ENVIADO NA FINALIZAÇÃO DA ASSINATURA
  $json = '{"nonce": "' . $nonce . '", "formatoDeDados": "Base64", "assinaturasPkcs1": [{"cifrado": "' . $cifrado . '", "nonce": "asdasdasd"}]}';

  // CRIA A REQUISIÇÃO DA FINALIZAÇÃO DA ASSINATURA
  $curlFinalizacao = curl_init();

  curl_setopt_array($curlFinalizacao, array(
    CURLOPT_URL => URLFinalizacao,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $json,
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/json",
      "Authorization:" . token
    ),
  ));

  // FAZ A REQUISIÇÃO DE FINALIZAÇÃO
  $respostaFinalizacao = curl_exec($curlFinalizacao);
  curl_close($curlFinalizacao);

  // IMPRIME A RESPOSTA DA FINALIZAÇÃO, QUE CONTÉM O LINK PARA DOWNLOAD DO ARQUIVO ASSINADO
  echo "RESPOSTA DA FINALIZAÇÃO:\n\n";
  echo $respostaFinalizacao;
  echo "\n\n__________________________________________________\n\n";
}


function getCertificadoEChavePrivada()
{
  // EXTRAI A CHAVE PRIVADA E O CONTEÚDO DO CERTIFICADO.
  $cert_store = file_get_contents(caminhoDoCertificado);
  var_dump(openssl_pkcs12_read($cert_store, $certs, senhaDoCertificado));
  $privkey = $certs['pkey'];
  $certificado = $certs['cert'];

  // RETIRA DA STRING DO CERTIFICADO PARTES NÃO DESEJADAS
  $certificadoTratado = str_replace('-----BEGIN CERTIFICATE-----', '', $certificado);
  $certificadoTratado = str_replace('-----END CERTIFICATE-----', '', $certificadoTratado);
  $certificadoTratado = str_replace("\n", '', $certificadoTratado);
  return array($certificadoTratado, $privkey);
}

function getContentRespostaInicializacao($respostaInicializacao)
{

  // MAPEIA O JSON QUE VOLTA COMO RESPOSTA DA REQUISIÇÃO
  $obj = json_decode($respostaInicializacao);
  // EXTRAI AS INFORMAÇÕES "messageDigest" e "nonce" DO RETORNO DA INICIALIZAÇÃO
  $messageDigest = $obj->assinaturasInicializadas[0]->messageDigest;
  $nonce = $obj->nonce;

  return array($messageDigest, $nonce);
}

$respostaInicializacao = inicializarAssinatura();
$cifrado = cifraInicialização($respostaInicializacao);
finalizarAssinatura($cifrado, $respostaInicializacao);
