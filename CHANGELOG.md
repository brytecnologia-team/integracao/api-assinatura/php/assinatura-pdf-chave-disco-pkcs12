# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 
Empty

## [1.0.1] - 2020-07-13

### Changed

- A SIGNATURE generation example refactor.

## [1.0.0] - 2020-07-03

### Added

- A SIGNATURE generation example.
- README
- CHANGELOG
- LICENSE

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/assinatura-pdf-chave-disco-pkcs12/-/tags/1.0.0
[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/assinatura-pdf-chave-disco-pkcs12/-/tags/1.0.1

